# cover.io

## Structure

This project is divided into 2 subprojects :  
* cover-front : a Vue.js frontend application
* cover-back : a Node.js backend application

## cover-front

### Project setup

First set the current working directory to cover-front : 

```
cd cover-front/
```

Then run the following command to install all node depedencies : 

```
npm install
```

#### Compiles and hot-reloads for development

```
npm run serve
```

#### Compiles and minifies for production

```
npm run build
```

#### Lints and fixes files

```
npm run lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
